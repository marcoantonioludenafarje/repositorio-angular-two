System.register(["../model/pelicula"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var pelicula_1;
    var PELICULAS;
    return {
        setters:[
            function (pelicula_1_1) {
                pelicula_1 = pelicula_1_1;
            }],
        execute: function() {
            exports_1("PELICULAS", PELICULAS = [
                new pelicula_1.Pelicula(1, "Batman y Robin 1 ", "Zack snider1", 2013),
                new pelicula_1.Pelicula(2, "Batman y Robin 2 ", "Zack snider2", 2014),
                new pelicula_1.Pelicula(3, "Batman y Robin 3 ", "Zack snider3", 2006),
                new pelicula_1.Pelicula(4, "Batman y Robin 4", "Zack snider4", 2016),
                new pelicula_1.Pelicula(5, "Batman y Robin 5", "Zack snider5", 2016),
                new pelicula_1.Pelicula(6, "Batman y Robin 5", "Zack snider5", 2016),
                new pelicula_1.Pelicula(7, "Dos tontos muy tontos ", "Zack snider5", 2016)
            ]);
        }
    }
});
//# sourceMappingURL=mock.peliculas.js.map