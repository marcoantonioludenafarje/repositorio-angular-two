// Importar el núcleo de Angular
import {Component} from 'angular2/core';
import {OnInit} from 'angular2/core';
import{Pelicula} from "../model/pelicula";
import{ROUTER_DIRECTIVES,Router,RouteParams} from "angular2/router";
import{PeliculasService} from "../services/peliculas.service";



// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    templateUrl:"app/view/crear-pelicula.html",
    providers:[PeliculasService],
    directives:[ROUTER_DIRECTIVES]
})

// Clase del componente donde iran los datos y funcionalidades
export class CrearPeliculaComponent implements OnInit {
    public TituloPelicula="";
    public nuevaPelicula:Pelicula;
    constructor(private _peliculaService:PeliculasService, private _router:Router, private _routeParams:RouteParams){

    }


    onSubmit(){
        // let pelicula:Pelicula= new Pelicula(77,titulo,director,anio);
        this._peliculaService.insertPelicula(this.nuevaPelicula);
        this._router.navigate(["Peliculas"]);
    }

    ngOnInit():any{
        this.TituloPelicula=this._routeParams.get("titulo");
        if (this._routeParams.get("anio")==null){
        this.nuevaPelicula=new Pelicula(0,this._routeParams.get("titulo"),this._routeParams.get("director"),null);

        }else{
        this.nuevaPelicula=new Pelicula(0,this._routeParams.get("titulo"),this._routeParams.get("director"),parseInt(this._routeParams.get("anio")));

        }

        }
}