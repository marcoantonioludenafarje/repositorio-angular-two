// Importar el núcleo de Angular
import {Component} from 'angular2/core';
import{Pelicula} from "../model/pelicula";
import{PeliculasService} from "../services/peliculas.service"
import {ROUTER_DIRECTIVES,RouteConfig,Router} from "angular2/router";
// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    selector: 'peliculas-list',
    templateUrl:"app/view/peliculas-list.html",
    providers:[PeliculasService],
    directives:[ROUTER_DIRECTIVES]
})
 
// Clase del componente donde iran los datos y funcionalidades
export class PeliculasListComponent {
    public pelicula:Pelicula;
    public peliculaElegida:Pelicula;
    public mostrarDatos:boolean;
    public peliculas; 
    public datoServicio;
    public postServicios;
    constructor(private _peliculasServices:PeliculasService){
        this.mostrarDatos=false;
        this.peliculas=this._peliculasServices.getPeliculas();
                this.pelicula= this.peliculas[0];
                this.peliculaElegida= this.peliculas[0];
    }

    onShowHide(value){
        this.mostrarDatos=value;

    }
    onlog(){
        console.log(this.pelicula)
    }
    onCambiarPelicula(pelicula){
        this.pelicula=pelicula;
        this.peliculaElegida=pelicula;
        
    }



}